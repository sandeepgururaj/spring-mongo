package com.springmongo.springmongo;

import com.apple.carrierservices.cspublisher.protocolbuffer.CsOrderEventPublishDetails;
import com.springmongo.springmongo.model.Interaction;
import com.springmongo.springmongo.model.Interactions;
import com.springmongo.springmongo.model.Message;
import com.springmongo.springmongo.repository.InteractionRepository;
import com.springmongo.springmongo.repository.MessageRepository;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;


@Service
public class InteractionService {

    private final ConversionService conversionService;
    private final InteractionRepository interactionRepository;
    private final MessageRepository messageRepository;

    public InteractionService(ConversionService mongoConversionService, InteractionRepository interactionRepository, MessageRepository messageRepository) {
        this.conversionService = mongoConversionService;
        this.interactionRepository = interactionRepository;
        this.messageRepository = messageRepository;
    }

    /*Rest Controller service methods*/
    public Flux<Interactions> getAllInteractions() {
        return interactionRepository.findAll();
    }
    public Mono<Interactions> getInteractionByOrderNumber(String orderNumber) {
        return interactionRepository.findById(orderNumber);
    }
    public Flux<Interactions> getInteractionsByOrderIssueType(String type) { return interactionRepository.getInteractionsByOrderIssueType(type); }
    public Flux<Interactions> getInteractionsByName(String name) { return interactionRepository.getInteractionsByName(name); }
    public Mono<Void> saveInteractions(Interactions interactions) { return interactionRepository.addNewInteractions(interactions); }
    /*Rest Controller service methods*/


    private Mono<Message> saveMessages(CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.Interaction interaction) {
        return messageRepository.save(conversionService.convert(interaction, Message.class));
    }

    public Mono<Void> saveMongoInteractions(CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.Interaction interaction) {
        return saveMessages(interaction)
            .flatMap(message -> Mono.fromCallable(() -> conversionService.convert(interaction, Interaction.class))
                .map(interactionDoc -> {
                    interactionDoc.setMessageId(message.getId());
                    return new Interactions(interaction.getWebserviceSessionId(), Collections.singletonList(interactionDoc));
                }).flatMap(interactions -> interactionRepository.addNewInteractions(interactions)));
    }
}
