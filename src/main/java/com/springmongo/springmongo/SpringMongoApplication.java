package com.springmongo.springmongo;

import com.mongodb.WriteConcern;
import com.mongodb.reactivestreams.client.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories
public class SpringMongoApplication {

	@Autowired
	MongoClient mongoClient;

	public static void main(String[] args) {
		SpringApplication.run(SpringMongoApplication.class, args);
	}

	@Bean
	public ReactiveMongoTemplate reactiveMongoTemplate() {
		ReactiveMongoTemplate reactiveMongoTemplate = new ReactiveMongoTemplate(mongoClient, "spring_mongo");
		reactiveMongoTemplate.setWriteConcern(WriteConcern.MAJORITY);
		return reactiveMongoTemplate;
	}

}
