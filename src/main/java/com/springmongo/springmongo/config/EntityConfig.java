package com.springmongo.springmongo.config;

import com.springmongo.springmongo.model.InteractionProtoToInteractionsDocumentConverter;
import com.springmongo.springmongo.model.InteractionProtoToMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;

@Configuration
public class EntityConfig {

    @Bean
    public ConversionService mongoConversionService() {
        DefaultConversionService conversionService = new DefaultConversionService();
        conversionService.addConverter(new InteractionProtoToMessageConverter());
        conversionService.addConverter(new InteractionProtoToInteractionsDocumentConverter());
        return conversionService;
    }
}
