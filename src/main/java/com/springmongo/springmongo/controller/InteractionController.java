package com.springmongo.springmongo.controller;

import com.springmongo.springmongo.InteractionService;
import com.springmongo.springmongo.model.Interactions;
import com.springmongo.springmongo.model.Message;
import com.springmongo.springmongo.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class InteractionController {

    @Autowired
    InteractionService interactionService;

    @Autowired
    MessageRepository messageRepository;

    @GetMapping("/interactions")
    public Flux<Interactions> getAllInteractions() {
        return interactionService.getAllInteractions();
    }

    @GetMapping("/messages/{id}")
    public Mono<Message> getMessageById(@PathVariable String id) {
        return messageRepository.findById(id);
    }

    @GetMapping("/interactions/{orderNumber}")
    public Mono<Interactions> getInteractionsByOrderNumber(@PathVariable String orderNumber) {
        return interactionService.getInteractionByOrderNumber(orderNumber);
    }

    @GetMapping("/interactions/issue/{type}")
    public Flux<Interactions> getInteractions(@PathVariable String type) {
        return interactionService.getInteractionsByOrderIssueType(type);
    }

    @GetMapping("/interactions/name/{name}")
    public Flux<Interactions> getInteractionsByName(@PathVariable String name) {
        return interactionService.getInteractionsByName(name);
    }

    @PostMapping("/interactions")
    public Mono<Void> createInteraction(@RequestBody Interactions interactions) {
        return interactionService.saveInteractions(interactions);
    }

    @PostMapping("/messages")
    public Mono<Message> createMessage(@RequestBody Message message) {
        return messageRepository.save(message);
    }
    
}
