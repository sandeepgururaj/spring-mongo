package com.springmongo.springmongo.kafka;

import com.apple.carrierservices.cspublisher.protocolbuffer.CsOrderEventPublishDetails;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InteractionEventDeserializer implements Deserializer<CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage> {

    private static final Logger LOGGER = LogManager.getLogger(InteractionEventDeserializer.class);

    @Override
    public CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage deserialize(final String topic, byte[] data) {
        try {
            return CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.parseFrom(data);
        } catch (InvalidProtocolBufferException e) {
            LOGGER.error("Received un-parse message exception and skip.");
            return null;
            // throw new RuntimeException("Received un-parse message " + e.getMessage(), e);
        }
    }
}
