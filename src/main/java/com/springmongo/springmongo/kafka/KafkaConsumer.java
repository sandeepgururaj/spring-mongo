package com.springmongo.springmongo.kafka;

import com.apple.carrierservices.cspublisher.protocolbuffer.CsOrderEventPublishDetails;
import com.springmongo.springmongo.InteractionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Consumer;

@Service
public class KafkaConsumer implements Consumer<Message<CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage>> {


    private final InteractionService interactionService;

    @Autowired
    public KafkaConsumer(InteractionService interactionService) {
        this.interactionService = interactionService;
    }

    @Override
    public void accept(Message<CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage> message) {
        CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage payload = message.getPayload();
        CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.InteractionsMessage interactionsMessage = payload.getInteractionsMessage();
        List<CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.Interaction> interactionList = interactionsMessage.getInteractionList();
        for (CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.Interaction interaction: interactionList) {
            interactionService.saveMongoInteractions(interaction).subscribe();
        }
    }
}
