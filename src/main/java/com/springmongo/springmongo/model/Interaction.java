package com.springmongo.springmongo.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;

@Data
@Builder
public class Interaction {
    String name;
    String messageId;
    String system;
    int duration;
    Date createdDt;
    @Field(value = "order_issue")
    OrderIssue orderIssue;
}
