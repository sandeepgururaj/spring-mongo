package com.springmongo.springmongo.model;

import org.springframework.core.convert.converter.Converter;
import com.apple.carrierservices.cspublisher.protocolbuffer.CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.Interaction;

import java.util.Date;

public class InteractionProtoToInteractionsDocumentConverter implements Converter<Interaction, com.springmongo.springmongo.model.Interaction> {
    @Override
    public com.springmongo.springmongo.model.Interaction convert(Interaction interaction) {

        return com.springmongo.springmongo.model.Interaction.builder()
            .name(interaction.getInteractionType())
            .createdDt(new Date(interaction.getCreatedDate()))
            .duration((int) interaction.getDuration())
            .orderIssue(OrderIssue.builder()
                        .type(interaction.getOrderIssue() != null ? interaction.getOrderIssue().getOrderIssueType() : "")
                        .detail(interaction.getOrderIssue() != null ? interaction.getOrderIssue().getOrderIssueDescription() : "")
                        .createdDt(interaction.getOrderIssue() != null ? new Date(interaction.getOrderIssue().getCreatedDate()) : null)
                        .build())
            .system(interaction.getSystemCode())
            .build();
    }
}
