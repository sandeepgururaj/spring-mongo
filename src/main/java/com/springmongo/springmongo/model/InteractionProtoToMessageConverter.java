package com.springmongo.springmongo.model;

import org.springframework.core.convert.converter.Converter;
import com.apple.carrierservices.cspublisher.protocolbuffer.CsOrderEventPublishDetails.CsOrderEventPublishDetailsMessage.Interaction;

public class InteractionProtoToMessageConverter implements Converter<Interaction, Message> {

    @Override
    public Message convert(Interaction interaction) {
        return Message.builder()
            .request(interaction.getRequest().getContent())
            .response(interaction.getResponse().getContent())
            .build();
    }
}
