package com.springmongo.springmongo.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


import java.util.List;

@Document("interactions")
public class Interactions {

    @Id
    String orderNumber;
    List<Interaction> details;

    public Interactions(String orderNumber, List<Interaction> details) {
        this.orderNumber = orderNumber;
        this.details = details;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List<Interaction> getDetails() {
        return details;
    }

    public void setDetails(List<Interaction> details) {
        this.details = details;
    }

}
