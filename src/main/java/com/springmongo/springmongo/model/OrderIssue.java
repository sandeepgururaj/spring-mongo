package com.springmongo.springmongo.model;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class OrderIssue {
    String type;
    String detail;
    Date createdDt;
}
