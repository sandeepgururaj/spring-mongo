package com.springmongo.springmongo.model;

import lombok.Data;

@Data
public class UnwindedInteraction {
    String id;
    Interaction details;
}
