package com.springmongo.springmongo.repository;

import com.springmongo.springmongo.model.Interactions;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomInteractionsRepository {
    Flux<Interactions> getInteractionsByOrderIssueType(String orderIssueType);
    Mono<Void> addNewInteractions(Interactions interactions);
    Flux<Interactions> getInteractionsByName(String interactionName);
}
