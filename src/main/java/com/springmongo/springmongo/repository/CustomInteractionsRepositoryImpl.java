package com.springmongo.springmongo.repository;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.unwind;

import com.springmongo.springmongo.model.Interactions;
import com.springmongo.springmongo.model.UnwindedInteraction;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Collections;

@Repository
public class CustomInteractionsRepositoryImpl implements CustomInteractionsRepository {

    private final ReactiveMongoTemplate mongoTemplate;
    private final ConversionService conversionService;

    public CustomInteractionsRepositoryImpl(ReactiveMongoTemplate mongoTemplate, ConversionService mongoConversionService) {
        this.conversionService = mongoConversionService;
        this.mongoTemplate = mongoTemplate;
    }


    @Override
    public Flux<Interactions> getInteractionsByOrderIssueType(String orderIssueType) {

        Aggregation aggregation = Aggregation.newAggregation(
            unwind("details"),
            match(Criteria.where("details.order_issue.type").is(orderIssueType))
        );

        return mongoTemplate
            .aggregate(aggregation, Interactions.class, UnwindedInteraction.class)
            .map(unwindedInteraction -> new Interactions(unwindedInteraction.getId(),
                Collections.singletonList(unwindedInteraction.getDetails())));
    }

    @Override
    public Flux<Interactions> getInteractionsByName(String interactionName) {
        Aggregation agg = Aggregation.newAggregation(
            unwind("details"),
            match(Criteria.where("details.name").is(interactionName))
        );

        return mongoTemplate
            .aggregate(agg, Interactions.class, UnwindedInteraction.class)
            .map(unwindedInteraction -> new Interactions(unwindedInteraction.getId(),
                Collections.singletonList(unwindedInteraction.getDetails())));

    }


    @Override
    public Mono<Void> addNewInteractions(Interactions interactions) {
        Query query = new Query(Criteria.where("orderNumber").is(interactions.getOrderNumber()));
        Update update = new Update().addToSet("details").each(interactions.getDetails());
        return mongoTemplate.upsert(query, update, Interactions.class).then();
    }

}
