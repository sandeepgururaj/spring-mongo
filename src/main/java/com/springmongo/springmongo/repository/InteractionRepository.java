package com.springmongo.springmongo.repository;

import com.springmongo.springmongo.model.Interactions;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface InteractionRepository extends ReactiveMongoRepository<Interactions, String>, CustomInteractionsRepository {
}
