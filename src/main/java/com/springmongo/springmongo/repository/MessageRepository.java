package com.springmongo.springmongo.repository;

import com.springmongo.springmongo.model.Message;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

public interface MessageRepository extends ReactiveMongoRepository<Message, String> {
}
